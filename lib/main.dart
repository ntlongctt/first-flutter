import 'package:flutter/material.dart';
import './pages/root_page.dart';
import './services/fire_base_auth.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Todo App',
      theme: new ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: new RootPage(auth: FireBaseAuth(),)
    );
  }
}