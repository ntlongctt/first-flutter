import 'package:flutter/material.dart';

class MultipleSeletectDialog extends StatefulWidget {
  final List<String> items;
  final List<String> selectedItems;
  final ValueChanged<List<String>> onSelectedChange;
  final String dialogTitle;

  const MultipleSeletectDialog(
      {Key key,
      this.items,
      this.selectedItems,
      this.onSelectedChange,
      this.dialogTitle})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MultipleSeletectDialogState();
  }
}

class _MultipleSeletectDialogState extends State<MultipleSeletectDialog> {
  List<String> _tempSelectedItem;
  String _dialogTitle;

  @override
  void initState() {
    _tempSelectedItem = widget.selectedItems;
    _dialogTitle = widget.dialogTitle;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Column(
        children: <Widget>[
          _showTitle(),
          _showContent(widget.items),
        ],
      ),
    );
  }

  Widget _showTitle() {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            _dialogTitle,
            style: TextStyle(fontSize: 20),
          ),
          FlatButton(
            child: Text('Done'),
            // color: Colors.blueGrey,
            onPressed: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
    );
  }

  Widget _showContent(List<String> items) {
    return Expanded(
      child: ListView.builder(
          itemCount: items.length,
          itemBuilder: (BuildContext context, int idx) {
            final item = items[idx];
            return Container(
              child: CheckboxListTile(
                title: Text(item),
                value: _tempSelectedItem.contains(item),
                onChanged: (bool value) {
                  if (value) {
                    if (!_tempSelectedItem.contains(item)) {
                      setState(() {
                        _tempSelectedItem.add(item);
                      });
                    }
                  } else {
                    if (_tempSelectedItem.contains(item)) {
                      setState(() {
                        _tempSelectedItem.remove(item);
                      });
                    }
                  }
                  widget.onSelectedChange(_tempSelectedItem);
                },
              ),
            );
          }),
    );
  }
}
