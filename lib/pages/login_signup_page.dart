import 'package:flutter/material.dart';
import '../services/fire_base_auth.dart';

enum FormMode { LOGIN, SIGNUP }

class LoginSignUpPage extends StatefulWidget {
  LoginSignUpPage({this.auth, this.onSignedIn});
  final FireBaseAuth auth;
  final VoidCallback onSignedIn;
  @override
  State<StatefulWidget> createState() {
    return _LoginSignUpPage();
  }
}

class _LoginSignUpPage extends State<LoginSignUpPage> {
  final _formKey = new GlobalKey<FormState>();

  bool _isLoading = false;
  bool _isIos;
  String _email;
  String _password;
  String _errorMessage = '';

  FormMode _formMode = FormMode.LOGIN;

  @override
  Widget build(BuildContext context) {
    _isIos = Theme.of(context).platform == TargetPlatform.iOS;
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Login Page"),
        ),
        body: Container(
          // height: 400,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/bg2.png'),
                  fit: BoxFit.fill)),
          child: new Stack(
            children: <Widget>[
              Positioned.fill(
                child: Container(
                  child: Stack(
                    children: <Widget>[
                      _showBody(),
                      _showCircularProgress(),
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
    // body: new Stack(
    //   children: <Widget>[_showBody(), _showCircularProgress()],
    // ));
  }

  Widget _showBody() {
    return new Container(
      padding: EdgeInsets.all(16),
      child: new Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            _showLogo(),
            _showEmailInput(),
            _showPassword(),
            _showPrimaryButton(),
            _showSecondaryButton(),
            _showErrorMessage()
          ],
        ),
      ),
    );
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(width: 0, height: 0);
  }

  Widget _showLogo() {
    return Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0, 70, 0, 0),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 48,
          child: Image.asset('assets/images/pika-cf.png'),
        ),
      ),
    );
  }

  Widget _showEmailInput() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 50, 0, 0),
      child: TextFormField(
        style: TextStyle(color: Colors.grey, fontSize: 17),
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: InputDecoration(
            hintText: 'Email', hintStyle: TextStyle(color: Colors.grey), icon: Icon(Icons.email, color: Colors.grey)),
        validator: (value) => value.isEmpty ? 'Please enter email' : null,
        onSaved: (value) => _email = value,
      ),
    );
  }

  Widget _showPassword() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
      child: TextFormField(
        style: TextStyle(color: Colors.grey),
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Password',
          hintStyle: TextStyle(color: Colors.grey),
          icon: Icon(Icons.lock, color: Colors.grey),
        ),
        validator: (value) => value.isEmpty ? 'Please enter password' : null,
        onSaved: (value) => _password = value,
      ),
    );
  }

  Widget _showPrimaryButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 45, 0, 0),
      child: MaterialButton(
        elevation: 5,
        minWidth: 200,
        height: 42,
        color: Colors.yellow[600].withOpacity(0.9),
        child: _formMode == FormMode.LOGIN
            ? Text('LOGIN', style: TextStyle(fontSize: 20, color: Colors.white))
            : Text('SIGN UP',
                style: TextStyle(fontSize: 20, color: Colors.white)),
        onPressed: _validateAndSubmit,
      ),
    );
  }

  Widget _showSecondaryButton() {
    return FlatButton(
      child: _formMode == FormMode.LOGIN
          ? Text('Create new account',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ))
          : Text('Login',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 20,
              )),
      onPressed: _changeFormMode,
    );
  }

  void _changeFormMode() {
    _formKey.currentState.reset();
    _errorMessage = '';
    setState(() {
      _formMode =
          _formMode == FormMode.LOGIN ? FormMode.SIGNUP : FormMode.LOGIN;
    });
  }

  Widget _showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 22,
            color: Colors.red,
            fontWeight: FontWeight.w300,
            height: 1),
      );
    } else {
      return Container(height: 0);
    }
  }

  // Check if form is valid before perform login or signup
  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  _validateAndSubmit() async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      String userId = "";
      try {
        if (_formMode == FormMode.LOGIN) {
          userId = await widget.auth.signIn(_email, _password);
          print('Signed in: $userId');
        } else {
          userId = await widget.auth.signUp(_email, _password);
          print('Signed up user: $userId');
        }
        if (userId.length > 0 && userId != null) {
          widget.onSignedIn();
        }
      } catch (e) {
        print('Error: $e');
        setState(() {
          _isLoading = false;
          if (_isIos) {
            _errorMessage = e.details;
          } else
            _errorMessage = e.message;
        });
      }
    }
  }
}
