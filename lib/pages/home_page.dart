import 'package:flutter/material.dart';
import 'package:myapp/pages/contacts_page.dart';
import 'package:myapp/pages/todos_page.dart';
import 'package:myapp/services/fire_base_auth.dart';
import '../pages/video_player.dart';


class HomePage extends StatefulWidget {
  final VoidCallback onSignedOut;
  final FireBaseAuth auth;

  HomePage({this.onSignedOut, this.auth});

  @override
  HomePageState createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  String _userId = "";

  @override
  void initState() {
    super.initState();
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        if (user != null) {
          _userId = user?.uid;
        }
      });
    });
  }

  _gotoTodoList() {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => TodoPage(
                auth: widget.auth,
                userId: _userId,
              )),
    );
  }

  _gotoContactList() {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ContactPage(
                userId: _userId,
              )),
    );
  }

  _gotoVideoPlayer() {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => VideoPlayerDemo()),
    );
  }

  _signOut() async {
    widget.onSignedOut();
  }

  Widget _showTodoItem() {
    return Card(
      color: Colors.white.withOpacity(0.2),
      child: ListTile(
        title: Text(
          'Todos',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        leading: Icon(
          Icons.view_list,
          color: Colors.white,
          size: 40,
        ),
        subtitle: Text(
          'View your todo list',
          style: TextStyle(color: Colors.white),
        ),
        onTap: () {
          _gotoTodoList();
        },
      ),
    );
  }

  Widget _showContactItem() {
    return Card(
      color: Colors.white.withOpacity(0.2),
      child: ListTile(
        title: Text(
          'Contacts',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        leading: Icon(
          Icons.contacts,
          color: Colors.white,
          size: 40,
        ),
        subtitle: Text(
          'View your contact list',
          style: TextStyle(color: Colors.white),
        ),
        onTap: () {
          _gotoContactList();
        },
      ),
    );
  }

  Widget _showVideoPlayer() {
    return Card(
      color: Colors.white.withOpacity(0.2),
      child: ListTile(
        title: Text(
          'video',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        leading: Icon(
          Icons.contacts,
          color: Colors.white,
          size: 40,
        ),
        subtitle: Text(
          'Video player demo',
          style: TextStyle(color: Colors.white),
        ),
        onTap: () {
          _gotoVideoPlayer();
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          'Home',
          style: TextStyle(color: Colors.white),
        ),
        actions: <Widget>[
          new FlatButton(
              child: new Text('Logout',
                  style: new TextStyle(fontSize: 17.0, color: Colors.white)),
              onPressed: () {
                _signOut();
              })
        ],
      ),
      body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/Blood.png'),
                  fit: BoxFit.cover)),
          child: Container(
            color: Colors.white30,
            child: ListView(
              children: <Widget>[
                _showTodoItem(),
                _showContactItem(),
                _showVideoPlayer()
              ],
            ),
          )),
    );
  }
}
