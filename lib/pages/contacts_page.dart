import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:myapp/models/contact.dart';
import 'package:myapp/pages/contact_detail.dart';
import '../services/fire_base_auth.dart';

class ContactPage extends StatefulWidget {
  ContactPage({Key key, this.auth, this.userId, this.onSignedOut})
      : super(key: key);

  final FireBaseAuth auth;
  final VoidCallback onSignedOut;
  final String userId;

  @override
  State<StatefulWidget> createState() => new _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  final FirebaseDatabase _database = FirebaseDatabase.instance;
  List<Contact> _contactList;
  Query _contactQuery;
  StreamSubscription<Event> _onTodoAddedSubscription;
  StreamSubscription<Event> _onTodoChangedSubscription;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        // backgroundColor: Colors.b,
        title: new Text(
          'Contact List',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage('assets/images/Blood.png'),
          fit: BoxFit.cover,
        )),
        child: new Stack(
          children: <Widget>[
            Positioned.fill(
              child: Container(
                child: Stack(
                  children: <Widget>[_showContactList(widget.userId)],
                ),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Add contact',
        child: Icon(Icons.group_add),
        onPressed: () {
          _addNewContact(widget.userId);
        },
      ),
    );
  }

  void _addNewContact(String userId) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ContactDetailPage(userId: userId)));
  }

  @override
  void initState() {
    super.initState();
    _contactList = new List();
    _contactQuery = _database
        .reference()
        .child("contact")
        .orderByChild("userId")
        .equalTo(widget.userId);

    _onTodoAddedSubscription = _contactQuery.onChildAdded.listen(_onEntryAdded);
    _onTodoChangedSubscription =
        _contactQuery.onChildChanged.listen(_onEntryChanged);
  }

  _onEntryChanged(Event event) {
    var oldEntry = _contactList.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });

    setState(() {
      _contactList[_contactList.indexOf(oldEntry)] =
          Contact.fromSnapshot(event.snapshot);
    });
  }

  _onEntryAdded(Event event) {
    setState(() {
      _contactList.add(Contact.fromSnapshot(event.snapshot));
    });
  }

  _goToContactDetail(String userId, Contact contact) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ContactDetailPage(userId: userId, contact: contact,)));
  }

  Widget _showContactList(String userId) {
    if (_contactList.length > 0) {
      return ListView.builder(
        shrinkWrap: true,
        itemCount: _contactList.length,
        itemBuilder: (BuildContext context, int index) {
          String name = _contactList[index].name;
          String contactId = _contactList[index].key;
          String imgUrl = _contactList[index].profileAvatar;
          String phone = _contactList[index] .phone;

          return Dismissible(
            key: Key(contactId),
            background: Container(color: Colors.red.withOpacity(0.3)),
            onDismissed: (direction) {},
            child: Card(
              color: Colors.white.withOpacity(0.5),
              child: ListTile(
                leading: Image.network(imgUrl, width: 50, height: 50,),
                title: Text(name, style: TextStyle(color: Colors.white, fontSize: 25),),
                onTap: () {
                  _goToContactDetail(userId, _contactList[index]);
                },
                subtitle: Text(phone, style: TextStyle(color: Colors.white),),
              ),
            ),
          );
        },
      );
    } else {
      return Center(
        child: Text('There are no contacts',
            textAlign: TextAlign.center, style: TextStyle(fontSize: 30, color: Colors.white)),
      );
    }
  }
}
