import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:flip_panel/flip_panel.dart';

class VideoPlayerDemo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _VideoPlayerState();
  }
}

class MyCustomCard extends StatelessWidget {
  MyCustomCard({this.colors});

  final MaterialColor colors;

  Widget build(BuildContext context) {
    return new Container(
      alignment: FractionalOffset.center,
      height: 144.0,
      width: 360.0,
      decoration: new BoxDecoration(
        color: colors.shade50,
        border: new Border.all(color: new Color(0xFF9E9E9E)),
      ),
      child: new FlutterLogo(size: 100.0, colors: colors),
    );
  }
}

class _VideoPlayerState extends State<VideoPlayerDemo>
    with TickerProviderStateMixin {
  VideoPlayerController _controller;
  AnimationController _aminController;
  Animation<double> _frontScale;
  Animation<double> _backScale;
  Offset _offset = Offset.zero; // changed

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(
        'https://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_20mb.mp4')
      ..initialize().then((_) {
        setState(() {});
      });

    _aminController = new AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    );

    _frontScale = new Tween(
      begin: 1.0,
      end: 0.8,
    ).animate(new CurvedAnimation(
      parent: _aminController,
      curve: new Interval(0.0, 0.5, curve: Curves.easeIn),
    ));
    _backScale = new CurvedAnimation(
      parent: _aminController,
      curve: new Interval(0.5, 1.0, curve: Curves.easeOut),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Video player'),
        ),
        body: flipCard(),
        floatingActionButton: flipCardBtn());
  }

  Widget flipCard() {
    return Transform(
      transform: Matrix4.skewY(0.0)..rotateZ(3.14 / 2),
      origin: Offset(50, 50),
      child: Container(
        height: 100.0,
        width: 200.0,
        color: Colors.cyan,
      ),
    );
  }

  Widget body() {
    return Scaffold(
        appBar: AppBar(
          title: Text('Video player'),
        ),
        body: flipTest(),
        floatingActionButton: flipCardBtn());
  }

  Widget custom(String text) {
    return Card(
      child: Text(text),
    );
  }

  Widget playVidBtn() {
    return FloatingActionButton(
      onPressed: () {
        setState(() {
          _controller.value.isPlaying
              ? _controller.pause()
              : _controller.play();
        });
      },
      child: Icon(
        _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
      ),
    );
  }

  Widget flipCardBtn() {
    return FloatingActionButton(
      child: new Icon(Icons.flip_to_back),
      onPressed: () {
        setState(() {
          if (_aminController.isCompleted || _aminController.velocity > 0)
            _aminController.reverse();
          else
            _aminController.forward();
        });
      },
    );
  }

  Widget video() {
    return Container(
      // padding: EdgeInsets.all(20),
      margin: EdgeInsets.all(20),
      // color: Colors.transparent,
      child: _controller.value.initialized
          ? ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(15)),
              child: AspectRatio(
                aspectRatio: _controller.value.aspectRatio,
                child: VideoPlayer(_controller),
              ),
            )
          : Container(),
    );
  }

  Widget flipTest() {
    final digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

    return FlipPanel.builder(
      itemBuilder: (context, index) => Container(
            color: Colors.black,
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Text(
              '${digits[index]}',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 100.0,
                  color: Colors.red),
            ),
          ),
      itemsCount: digits.length,
      period: const Duration(milliseconds: 1000),
      loop: 10,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}
