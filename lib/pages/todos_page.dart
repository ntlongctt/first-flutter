import 'dart:async';

import 'package:flutter/material.dart';
import 'package:myapp/pages/app_layout.dart';
import '../services/fire_base_auth.dart';
import '../models/todo.dart';
import 'package:firebase_database/firebase_database.dart';

class TodoPage extends StatefulWidget {
  TodoPage({Key key, this.auth, this.userId}) : super(key: key);

  final FireBaseAuth auth;
  // final VoidCallback onSignedOut;
  final String userId;

  @override
  State<StatefulWidget> createState() => new _TodoPageState();
}

class _TodoPageState extends State<TodoPage> {
  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final TextEditingController _textEditingController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldState = new GlobalKey<ScaffoldState>();

  List<Todo> _todoList = [];
  Query _todoQuery;
  StreamSubscription<Event> _onTodoAddedSubscription;
  StreamSubscription<Event> _onTodoChangedSubscription;

  _onEntryChanged(Event event) {
    var oldEntry = _todoList.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });

    setState(() {
      _todoList[_todoList.indexOf(oldEntry)] =
          Todo.fromSnapshot(event.snapshot);
    });
  }

  _onEntryAdded(Event event) {
    setState(() {
      _todoList.add(Todo.fromSnapshot(event.snapshot));
    });
  }

  Widget _showTodoList() {
    if (_todoList.length > 0) {
      return ListView.builder(
        shrinkWrap: true,
        itemCount: _todoList.length,
        itemBuilder: (BuildContext context, int index) {
          String todoId = _todoList[index].key;
          String subject = _todoList[index].subject;
          bool completed = _todoList[index].completed;

          return Dismissible(
              key: Key(todoId),
              background: Container(color: Colors.red.withOpacity(0.3)),
              onDismissed: (direction) async {
                _deleteTodo(todoId, index);
              },
              child: Card(
                color: Colors.white.withOpacity(0.2),
                child: ListTile(
                  title: Text(
                    subject,
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  ),
                  trailing: IconButton(
                    icon: completed
                        ? Icon(Icons.done_outline,
                            color: Colors.green, size: 20)
                        : Icon(
                            Icons.done,
                            color: Colors.grey,
                            size: 20,
                          ),
                    onPressed: () {
                      _updateTodo(_todoList[index]);
                    },
                  ),
                ),
              ));
        },
      );
    } else {
      return Center(
        child: Text('Great! You have done all job',
            textAlign: TextAlign.center, style: TextStyle(fontSize: 30)),
      );
    }
  }

  _deleteTodo(String todoId, int index) {
    _database.reference().child('todo').child(todoId).remove().then((_) {
      setState(() {
        _todoList.removeAt(index);
      });
      _showDeleteSuccessMsg();
    });
  }

  _updateTodo(Todo todo) {
    todo.completed = !todo.completed;
    if (todo != null) {
      _database.reference().child('todo').child(todo.key).set(todo.toJson());
    }
  }

  _addTodo(String subject) {
    if (subject.length > 0) {
      Todo todo = new Todo(widget.userId, subject, false);
      _database.reference().child('todo').push().set(todo.toJson())
        .then((_) => _showAddSuccessMsg());
    }
  }

  _showDialog(BuildContext context) async {
    _textEditingController.clear();
    await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Add new todo:'),
            content: Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    controller: _textEditingController,
                    autofocus: true,
                    decoration: InputDecoration(hintText: 'Subject...'),
                  ),
                )
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Cancel', style: TextStyle(color: Colors.grey)),
                onPressed: () => Navigator.pop(context),
              ),
              FlatButton(
                child: Text('Save'),
                onPressed: () {
                  _addTodo(_textEditingController.text.toString());
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  _showDeleteSuccessMsg() {
    final _deleteSuccessMsg = SnackBar(
      content: Text(
        'Todo item deketed!',
        style: TextStyle(fontSize: 17),
      ),
      backgroundColor: Colors.green,
    );
    _scaffoldState.currentState.showSnackBar(_deleteSuccessMsg);
  }

  _showAddSuccessMsg() {
    final _deleteSuccessMsg = SnackBar(
      content: Text(
        'New todo added!',
        style: TextStyle(fontSize: 17),
      ),
      backgroundColor: Colors.green,
    );
    _scaffoldState.currentState.showSnackBar(_deleteSuccessMsg);
  }

  @override
  void dispose() {
    _onTodoAddedSubscription.cancel();
    _onTodoChangedSubscription.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _todoList = new List();
    _todoQuery = _database
        .reference()
        .child("todo")
        .orderByChild("userId")
        .equalTo(widget.userId);
    _onTodoAddedSubscription = _todoQuery.onChildAdded.listen(_onEntryAdded);
    _onTodoChangedSubscription =
        _todoQuery.onChildChanged.listen(_onEntryChanged);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldState,
      appBar: new AppBar(
        // backgroundColor: Colors.b,
        title: new Text(
          'Todo list',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/Blood.png'),
                fit: BoxFit.cover)),
        child: new Stack(
            children: <Widget>[
              Positioned.fill(
                child: Container(
                  child: Stack(
                    children: <Widget>[
                     _showTodoList()
                    ],
                  ),
                ),
              )
            ],
          ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _showDialog(context),
        tooltip: 'Add todo',
        child: Icon(Icons.add),
      ),
    );
    // return AppLayout(
    //   'Todo page',
    //   _showTodoList(),
    // );
  }
}
