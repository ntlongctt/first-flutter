import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:myapp/models/contact.dart';
import 'package:uuid/uuid.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import '../customs/controls/dialog_multiSelect.dart';

class ContactDetailPage extends StatefulWidget {
  final String userId;
  final Contact contact;
  ContactDetailPage({this.userId, this.contact});

  @override
  ContactDetailPageState createState() {
    return new ContactDetailPageState();
  }
}

class ContactDetailPageState extends State<ContactDetailPage> {
  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldState =
      new GlobalKey<ScaffoldState>();

  File _image;
  Contact _contact;
  List<String> _selectedItem;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    print(image.path);
    setState(() {
      _image = image;
    });
  }

  @override
  void initState() {
    super.initState();
    _contact = widget.contact != null
        ? widget.contact
        : new Contact('', '', '', '', '', widget.userId, '', '', []);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      appBar: AppBar(
        title: Text('Contact detail'),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              _submitForm(widget.userId);
            },
            child: Text(
              'Save',
              style: TextStyle(color: Colors.white, fontSize: 17),
            ),
          )
        ],
      ),
      body: Container(
          padding: EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                _avarta(),
                _showContactName(),
                _showContactEmail(),
                _showPhoneNumber(),
                _showDob(),
                _showAddress(),
                _showGender(),
                _showFavorite(),
              ],
            ),
          )),
    );
  }

  Widget _avarta() {
    return Container(
        // color: Colors.green,
        height: 200,
        width: 200,
        margin: EdgeInsets.fromLTRB(80, 0, 80, 0),
        child: FlatButton(
          // color: Colors.red.withOpacity(0.5),
          onPressed: () {
            _pickAvarta(widget.userId);
          },
          child: Card(
            // color: Colors.green,
            margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
            child: _showImage(),
          ),
        ));
  }

  Widget _showImage() {
    if (_contact != null && _contact.profileAvatar.length > 0) {
      return Image.network(_contact.profileAvatar);
    } else {
      return _image == null ? Text('No Image') : Image.file(_image);
    }
  }

  Widget _showContactName() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
      child: TextFormField(
        initialValue: (_contact != null) ? _contact.name : '',
        style: TextStyle(color: Colors.grey, fontSize: 17),
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: InputDecoration(
            hintText: 'Name',
            hintStyle: TextStyle(color: Colors.grey),
            icon: Icon(Icons.short_text, color: Colors.grey)),
        validator: (value) =>
            value.isEmpty ? 'Please enter contact name' : null,
        onSaved: (value) => _contact.name = value,
      ),
    );
  }

  Widget _showContactEmail() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
      child: TextFormField(
        initialValue: _contact != null ? _contact.email : '',
        style: TextStyle(color: Colors.grey, fontSize: 17),
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: InputDecoration(
            hintText: 'Email',
            hintStyle: TextStyle(color: Colors.grey),
            icon: Icon(Icons.short_text, color: Colors.grey)),
        validator: (value) => value.isEmpty ? 'Please enter Email' : null,
        onSaved: (value) => _contact.email = value,
      ),
    );
  }

  Widget _showDob() {
    return Padding(
        padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
        child: DateTimePickerFormField(
          initialValue: _contact != null
              ? ((_contact.dob != 'null' && _contact.dob.length > 0)
                  ? DateTime.parse(_contact.dob)
                  : DateTime.now())
              : DateTime.now(),
          format: DateFormat('dd-MM-yyyy'),
          inputType: InputType.date,
          editable: true,
          decoration: InputDecoration(
              hintText: 'Date of birth',
              hintStyle: TextStyle(color: Colors.grey),
              icon: Icon(Icons.calendar_today, color: Colors.grey)),
          onChanged: (dt) => setState(() => _contact.dob = dt.toString()),
        ));
  }

  Widget _showPhoneNumber() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
      child: TextFormField(
        initialValue: _contact != null ? _contact.phone : '',
        style: TextStyle(color: Colors.grey, fontSize: 17),
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: InputDecoration(
            hintText: 'Phone number',
            hintStyle: TextStyle(color: Colors.grey),
            icon: Icon(Icons.phone, color: Colors.grey)),
        validator: (value) =>
            value.isEmpty ? 'Please enter phone number' : null,
        onSaved: (value) => _contact.phone = value,
      ),
    );
  }

  Widget _showAddress() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
      child: TextFormField(
        initialValue: _contact != null ? _contact.address : '',
        style: TextStyle(color: Colors.grey, fontSize: 17),
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: InputDecoration(
            hintText: 'Address',
            hintStyle: TextStyle(color: Colors.grey),
            icon: Icon(Icons.home, color: Colors.grey)),
        validator: (value) => value.isEmpty ? 'Please enter address' : null,
        onSaved: (value) => _contact.address = value,
      ),
    );
  }

  Widget _showGender() {
    return Padding(
        padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
        child: DropdownButtonFormField(
          value: _contact.gender.length > 0 ? _contact.gender : null,
          decoration: InputDecoration(
              hintText: 'Gender',
              hintStyle: TextStyle(color: Colors.grey),
              icon: Icon(Icons.face, color: Colors.grey)),
          items: ['Male', 'Female'].map((item) {
            return DropdownMenuItem(
              value: item,
              child: Text(item),
            );
          }).toList(),
          onChanged: (newVal) {
            setState(() {
              _contact.gender = newVal;
            });
          },
        ));
  }

  Widget _showFavorite() {
    return Padding(
        padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
        child: RaisedButton(
          child: Text(
            'Favorite: ',
            style: TextStyle(color: Colors.white),
          ),
          color: Colors.grey,
          onPressed: () {
            _showFavoriteDialog();
          },
        ));
  }

  _showFavoriteDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return MultipleSeletectDialog(
            dialogTitle: 'Choose movie list',
            items: [
              'Overlord',
              'Sword art onlint',
              'Doreamon',
              '5cm/s',
              'Conan',
              'Lord of the ring',
              'Srat war',
              'Avenger',
              'Superman vs Batman',
              'Dark knight'
            ],
            selectedItems: (_contact.favoritesMovie != null) ? _contact.favoritesMovie : [],
            onSelectedChange: (items) {
              _selectedItem = items;
              _contact.favoritesMovie = items;
              print(items);
            },
          );
        });
  }

  void _pickAvarta(String userId) {
    print('_pickAvarta');
    getImage().then((_) {
      print(_image.path);
      _uploadImage(userId, _image).then((url) {
        _contact.profileAvatar = url;
        if (_contact.key != null && _contact.key.length > 0) {
          setState(() {
            _updateContact(_contact);
          });
        }
      });
    });
  }

  Future<String> _uploadImage(String imageId, File imageFile) async {
    // File imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    var uuid = new Uuid();
    var _profileImageId = uuid.v1();
    StorageReference ref = FirebaseStorage.instance
        .ref()
        .child(imageId)
        .child("$_profileImageId.jpg");
    StorageUploadTask uploadTask = ref.putFile(imageFile);
    return await (await uploadTask.onComplete).ref.getDownloadURL();
  }

  void _submitForm(String userId) {
    if (_validate()) {
      if (_contact.key != null) {
        _updateContact(_contact);
      } else {
        _addContact(_contact);
      }
    }
  }

  void _addContact(Contact contact) {
    _database
        .reference()
        .child('contact')
        .push()
        .set(contact.toJson())
        .then((_) => _showSuccessMsg('Add contact success!'));
  }

  void _updateContact(Contact contact) {
    _database
        .reference()
        .child('contact')
        .child(contact.key)
        .set(contact.toJson())
        .then((_) => _showSuccessMsg('Update contact success!'));
  }

  // Check if form is valid before perform login or signup
  bool _validate() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  _showSuccessMsg(String message) {
    final msg = SnackBar(
      content: Text(
        message,
        style: TextStyle(fontSize: 17),
      ),
      backgroundColor: Colors.green,
    );
    _scaffoldState.currentState.showSnackBar(msg);
  }
}
