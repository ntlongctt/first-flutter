import 'package:flutter/material.dart';
import 'package:myapp/services/fire_base_auth.dart';

class AppLayout extends StatefulWidget {

  FireBaseAuth auth;
  String _title;
  Widget _body;
  Widget _action;
  
  AppLayout(this._title, this._body);

  @override
  AppLayoutState createState() {
    return new AppLayoutState();
  }
}

class AppLayoutState extends State<AppLayout> {

  _signOut() async {
     try {
      await widget.auth.signOut();
      // widget.onSignedOut();
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.blue,
        title: new Text(
          widget._title,
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/Blood.png'),
            fit: BoxFit.cover
          )
        ),
        child: widget._body,
      )
    );
  }
}