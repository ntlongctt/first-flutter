import 'package:firebase_database/firebase_database.dart';

class Contact {
  String key;
  String name;
  String phone;
  String address;
  String dob;
  String profileAvatar;
  String userId;
  String email;
  String gender;
  List<String> favoritesMovie;

  Contact(this.name, this.phone, this.address, this.dob, this.profileAvatar,
      this.userId, this.email, this.gender, this.favoritesMovie);

  Contact.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        name = snapshot.value["name"],
        phone = snapshot.value["phone"],
        address = snapshot.value["address"],
        dob = snapshot.value["dob"],
        profileAvatar = snapshot.value["profileAvatar"],
        userId = snapshot.value["userId"],
        email = snapshot.value["email"],
        gender = snapshot.value["gender"],
        favoritesMovie = snapshot.value["favoritesMovie"];

  toJson() {
    return {
      'name': name,
      'phone': phone,
      'address': address,
      'dob': dob,
      'profileAvatar': profileAvatar,
      'userId': userId,
      'email': email,
      'gender': gender,
      'favoritesMovie': favoritesMovie,
    };
  }
}
